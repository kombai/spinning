# Electron Spinning App
## Require

* NodeJs 6.x

* NPM 3.x

* Gulp 3.x

* Electron v1.4.0

## Install

$ npm install -g electron

$ git clone https://gitlab.com/kombai/spinning.git Spinning

$ cd Spinning

$ npm install

## Run

$ gulp
