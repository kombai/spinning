import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';

class EditLeaf extends Component {

    constructor(props, context) {
        super(props);

        this.state = {
			leaf: {}
        }
    }

    componentDidMount() {
        let location = this.props.location;
        let leaf = location.state;
        this.setState({
            leaf: leaf
        });
    }

	render() {
        return (
            <Planting
                mode="update"
                leaf={this.state.leaf}
            />
        );
	}
}

export default EditLeaf
