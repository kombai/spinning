import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helper from '../../../common/helper';
import LeafAction from '../connect/action';

class ListLeaf extends Component {

    constructor(props, context) {
        super(props);

        this.state = {
            leaves: []
        }
        this.findLeaf = this.findLeaf.bind(this);
    }

    componentWillMount() {
		this.props.loadData();
	}

    componentWillReceiveProps(newProps) {
		if (newProps.type == "LOAD-LEAF" || newProps.type == "FIND-LEAF") {
			this.setState({
				leaves: newProps.leaves
			});
		} else {
            // when update leaf;
			this.props.loadData();
		}
    }

    findLeaf(event) {
		this.props.findData(event.target.value);
	}

	render() {
        return (
            <div className="list-leaf page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <div className="pull-right">
                            <Link to={{pathname: "create-leaf", state: null}}>
                                <button className="btn btn-success">Create a leaf</button>
                            </Link>
                        </div>
                        <h2>List LEAF</h2>
                        <div className="form-group has-feedback">
                            <input type="text" className="form-control" placeholder="search" onChange={this.findLeaf}/>
                            <i className="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </div>
                    <div className="page-body">
                        <div className="list-content">
                            {
        						this.state.leaves.map(function(item, index) {
                                    let customeStyle = {};
                                    if (item.type == "image") {
                                        customeStyle = {
                                            backgroundImage: "url(" + Helper.getImagePath(item.value) + ")"
                                        };
                                    }
        							return (
        								<div key={item._id} className="leaf">
                                            <div className="thumbnail">
                                                <div className="photo" style={customeStyle}></div>
                                                <div className="caption">
                                                    <Link to={{pathname: "edit-leaf/" + item._id, state: item}}>{item.name}</Link>
                                                </div>
                                            </div>
        								</div>
        							);
        						}.bind(this))
        					}
                        </div>
                    </div>
                </div>
                <div className="page-extend"></div>
            </div>
		);
	}
}

function mapStateToProps(fullState, thisProps) {
	return fullState.leaf;
}

const mapAction = (dispatch) => {
	return {
        loadData: () => {
            LeafAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			LeafAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(ListLeaf)
