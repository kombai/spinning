import React, {Component, PropTypes} from 'react';
import TwigStore from './tiny-store';
import TinyAction from './tiny-action';

class LeafHead extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            leafName: ""
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            leafName: newProps.leafName
        });
    }

    componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TwigStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let twigState = TwigStore.getState();
        let actionType = twigState.type;

        if (actionType == "UPDATE-LEAF") {
            this.setState({
                leafName: twigState.leafName
            });
            this.props.setLeafData('leafName', twigState.leafName);
        }
    }

    selectLeaf() {
        TinyAction.selectLeaf(this.state.leafName);
    }

	render() {
		return (
            <section onClick={() => this.selectLeaf()}>
                <div className="pull-right add-new">
                    <span className="big-text glyphicon glyphicon-pencil"></span>
                    <span>...</span>
                </div>
    			<h2>
                    {this.state.leafName}
    		    </h2>
            </section>
		);
	}
}


LeafHead.contextTypes = {
    router: PropTypes.object.isRequired
};

LeafHead.propTypes = {
	leafName: PropTypes.string,
    setLeafData: PropTypes.func
};

export default LeafHead
