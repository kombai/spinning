import React, {Component, PropTypes} from 'react';
import TinyAction from './tiny-action';
import TinyStore from './tiny-store';


class SelectLeaf extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			visible: false,
			currentText: ""
		};

		this.onTextChange = this.onTextChange.bind(this);

		this.clearLeafName = this.clearLeafName.bind(this);
		this.applyRemoveLeaf = this.applyRemoveLeaf.bind(this);
		this.applyUpdateLeaf = this.applyUpdateLeaf.bind(this);
		this.cancelSelectLeaf = this.cancelSelectLeaf.bind(this);
	}

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let actionType = plantingState.type;

		if (actionType == "SELECT-LEAF") {
			this.state.visible = true;
        } else {
			this.state.visible = false;
		}

		this.setState({
			currentText: plantingState.leafName
		});
    }

	onTextChange(event) {
		this.setState({
            currentText: event.target.value
        });
	}

	cancelSelectLeaf() {
		this.setState({
			visible: false
		});
	}

	clearLeafName() {
		this.setState({
			currentText: ""
		});
	}

	applyUpdateLeaf() {
		let leafName = this.state.currentText;
		if (leafName.trim() != "") {
			TinyAction.updateLeaf(leafName);
		}
	}

	applyRemoveLeaf() {
		let leafName = this.state.currentText;
		if (leafName.trim() == "") {
			this.props.removeLeaf();
		}
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let currentText = this.state.currentText;
		let require = currentText ? "" : "require";

		let isEdit = false;
		if (this.props.mode === "update") {
			isEdit = true;
		}

		return (
			<div className="modal select-leaf">
				<div className="content">
					<div className="modal-header">
						<button className="btn btn-default pull-right" onClick={this.cancelSelectLeaf}>Close</button>
						<button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateLeaf}>Apply</button>
						<h3 className="modal-title">{isEdit ? "Edit Leaf" : "Create Leaf"}</h3>
					</div>
					<div className="modal-body">
						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearLeafName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveLeaf}></i>
										:
										null
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SelectLeaf.contextTypes = {
    router: PropTypes.object.isRequired
}

SelectLeaf.propTypes = {
	mode: PropTypes.string,
    removeLeaf: PropTypes.func
}

export default SelectLeaf;
