
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';

const TinyAction = {

    selectLeaf(leafName) {
        TinyStore.dispatch({
            type: "SELECT-LEAF",
            leafName: leafName
        });
    },

    updateLeaf(leafName) {
        TinyStore.dispatch({
            type: "UPDATE-LEAF",
            leafName: leafName
        });
    }
}

export default TinyAction;
