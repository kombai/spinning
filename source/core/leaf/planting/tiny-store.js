import {createStore} from 'redux';

let tinyState = {
    type: "",
    leafName: ""
}

function reducer(state = tinyState, action) {
    switch (action.type) {
        case 'SELECT-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                leafName: action.leafName
            });

        case 'UPDATE-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                leafName: action.leafName
            });

        default:
            return state
    }
}

export default createStore(reducer)
