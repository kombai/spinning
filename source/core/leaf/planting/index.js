import React, {Component, PropTypes} from 'react';
import LeafAction from '../connect/action';
import {connect} from 'react-redux';
import Helper from '../../../common/helper';

import HeadLeaf from "./head-leaf";
import LeafImage from "./leaf-image";
import SelectLeaf from './select-leaf';

class Planting extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            leafType: "none", // none, image, service
            mode: "", // insert, update,
            leafObj: {},
            leafId: "",
			leafName: "",
            imagePath: ""
        };

        //binding event;
        this.removeLeaf = this.removeLeaf.bind(this);
        this.setLeafData = this.setLeafData.bind(this);
        this.onTypeChange = this.onTypeChange.bind(this);
        this.setImagePath = this.setImagePath.bind(this);
    }

    componentWillReceiveProps(newProps) {
        let leaf = newProps.leaf;
        // image type
        if (leaf.type === "image") {
            this.state.imagePath = Helper.getImagePath(leaf.value);
        } else if (leaf.type == "") {
            leaf.type = "none";
        }

        this.setState({
            mode: newProps.mode,
            leafObj: leaf,
            leafId: leaf._id,
            leafType: leaf.type,
            leafName: leaf.name
        });
    }

    saveLeaf() {
        let imagePath = this.state.imagePath;
        let leafName = this.state.leafName;
        let leafType = this.state.leafType;
        let leafId = this.state.leafId;
        let leafValue = "";

        if (!leafName) {
            alert("The name of leaf is require !");
            return;
        }

        if (leafType === "image") {
            if (imagePath == "") {
                leafType = "none";
            } else {
                leafValue = leafId.replace(/-/g, "/") + ".png";
            }
        } else {
            leafValue = leafName;
        }

        let leafObj = {
            _id: leafId,
            name: leafName,
            type: leafType,
            value: leafValue
        };

        if (this.state.mode === "insert") {
            this.props.insert(leafObj, imagePath);
            // change mode;
            this.setState({
                mode: "update"
            });
        } else if (this.state.mode === "update") {
            this.props.update(leafObj, imagePath);
        }

        setTimeout(() => {
            this.context.router.goBack();
        }, 345);
    }

    removeLeaf() {
        if (this.state.mode == "update") {
            let leafObj = this.state.leafObj;
            this.props.remove(leafObj);
            setTimeout(() => {
                this.context.router.goBack();
            }, 345);
        }
    }

    onTypeChange(event) {
        let leafType = event.target.value;
        this.setState({
            leafType: leafType
        });
    }

    setImagePath(value) {
        this.setState({
            imagePath: value
        });
    }

    setLeafData(propType, propData) {
        this.state[propType] = propData;
    }

	render() {
		return (
			<div className="leaf-page page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <HeadLeaf
                            leafName={this.state.leafName}
                            setLeafData={this.setLeafData}
                        ></HeadLeaf>
                    </div>
                    <div className="page-body">
                            <div className="form-group">
                                <div className="big-text">Leaf type</div>
                                <div className="type-group">
                                    <label className="radio-inline">
                                        <input type="radio" value="none" checked={this.state.leafType === 'none'}  onChange={this.onTypeChange} />None
                                    </label>
                                    <label className="radio-inline">
                                        <input type="radio" value="image" checked={this.state.leafType === 'image'} onChange={this.onTypeChange} />Image
                                    </label>
                                    <label className="radio-inline">
                                        <input type="radio" value="service" checked={this.state.leafType === 'service'} onChange={this.onTypeChange} />Service
                                    </label>
                                </div>
                            </div>
                            <div className="form-group">
                                {
                                    this.state.leafType === "image" ?
                                    (<LeafImage
                                        imagePath={this.state.imagePath}
                                        setImagePath={this.setImagePath}
                                    />) : null
                                }
                            </div>
                    </div>

                    <div className="page-footer">
                        <button className="btn btn-lg btn-success" onClick={() => this.saveLeaf()}>Save</button>
                        <button className="btn btn-lg btn-warning pull-right" onClick={() => this.context.router.goBack()}>Cancel</button>
    				</div>

                </div>
                <div className="page-extend">
                    <SelectLeaf
                        mode={this.state.mode}
                        removeLeaf={this.removeLeaf}
                    ></SelectLeaf>
                </div>
		    </div>
		);``
	}
}


Planting.contextTypes = {
    router: PropTypes.object.isRequired
}

const mapAction = (dispatch) => {
	return {
        insert: (leaf, sourcePath) => {
            LeafAction.insert(dispatch, leaf, sourcePath);
        },
        update: (leaf, sourcePath) => {
            LeafAction.update(dispatch, leaf, sourcePath);
        },
        remove: (leaf) => {
            LeafAction.remove(dispatch, leaf);
        }
  	}
}

export default connect(null, mapAction)(Planting)
