import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helper from '../../../common/helper';
const electronRemote = electronRequire("electron").remote;

class LeafImage extends Component {

	constructor(props, context) {
        super(props, context);

        //biding event;
        this.selectImage = this.selectImage.bind(this);
        this.removeImage = this.removeImage.bind(this);
    }

	selectImage() {
        let showDialog = electronRemote.dialog.showOpenDialog;
        showDialog({
            filters: [
                {name: 'Images', extensions: ['jpg', 'png']}
            ]
        }, function (fileNames) {
            if (fileNames && fileNames.length) {
                this.props.setImagePath(fileNames[0]);
            }
        }.bind(this));
    }

    removeImage() {
        this.props.setImagePath("");
    }

	render() {
		let customeStyle = {
			backgroundImage: "url(" + this.props.imagePath + ")"
		};

		return (
			<div className="leaf-image" style={this.props.imagePath ? customeStyle : null}>
                <div className="btn-group" >
					<button className="btn btn-success" onClick={this.selectImage}>Select photo</button>
					{
						this.props.imagePath ?
						<button className="btn btn-danger" onClick={this.removeImage}><span className="glyphicon glyphicon-remove"></span></button>
						: null
					}
				</div>
		    </div>
		);
	}
}

LeafImage.propTypes = {
    imagePath: PropTypes.string,
	setImagePath: PropTypes.func
}

LeafImage.contextTypes = {
    router: PropTypes.object.isRequired
}


export default LeafImage
