import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';

class CreateLeaf extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            leaf: {}
        };
    }

    componentDidMount() {
        this.setState({
            leaf: {
                _id: Helper.getGUID(),
                name: "New Leaf",
                type: "none" // none, image, service
            }
        });
    }

	render() {
		return (
			<Planting
                mode="insert"
                leaf={this.state.leaf}
            />
		);
	}
}


export default CreateLeaf
