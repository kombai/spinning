const fs = electronRequire("fs-extra");
import Helper from '../../../common/helper';
import dataStore from './store-data';

const leafAction = {

    loadData(dispatch) {
        dataStore.find({} , function(err, docs) {
			dispatch({
                type: "LOAD-LEAF",
                leaves: docs
            });
		});
    },

    findData(dispatch, textSearch) {
        dataStore.find({name: new RegExp(textSearch, 'i')} , function(err, docs) {
            dispatch({
                type: "FIND-LEAF",
                leaves: docs
            });
		});
    },

    insert(dispatch, leaf, sourcePath) {
        if (leaf.type == "image") {
            let localPath = Helper.getImagePath(leaf.value);
            fs.copy(sourcePath, localPath, function (err) {
                if (err) return console.error(err);
                console.log("copy done!");
            });
        }

        dataStore.insert(leaf, function(err, newDoc) {
            dispatch({
                type: "INSERT-LEAF"
            });
        });
    },

    update(dispatch, leaf, sourcePath) {
        let localPath = Helper.getImagePath(leaf.value);
        if (leaf.type == "image") {
            // only update with new image;
            if (sourcePath != localPath) {
                fs.copy(sourcePath, localPath, function (err) {
                    if (err) return console.error(err);
                    console.log("update done!");
                });
            }
        } else {
            // remove exists value;
            fs.remove(localPath, function (err) {
                if (err) return console.error(err);
                console.log("remove done!");
            });
        }

        dataStore.update({_id: leaf._id }, {$set: leaf}, {}, function (err) {
            dispatch({
                type: "UPDATE-LEAF"
            });
        });
    },

    remove(dispatch, leaf) {
        if (leaf.type === "image") {
            let localPath = Helper.getImagePath(leaf.value);
            fs.remove(localPath, function (err) {
                if (err) return console.error(err);
                console.log("remove done!");
            });
        }
        dataStore.remove({_id: leaf._id}, {}, function(err) {
            dispatch({
                type: "REMOVE-LEAF"
            });
        });
    }
}

export default leafAction
