let Datastore = electronRequire('nedb');

let LeafData = new Datastore({
    filename: './storage/data-store/leaf-data.db'
});

export default LeafData;
