import leafState from './state'

function reducer(state = leafState, action) {
    switch (action.type) {
        case 'LOAD-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                leaves: action.leaves
            });
        case 'FIND-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                leaves: action.leaves
            });
        case 'INSERT-LEAF':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'UPDATE-LEAF':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'REMOVE-LEAF':
            return Object.assign({}, state, {
                type: action.type
            });
        default:
            return state
    }
}

export default reducer
