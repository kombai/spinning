import twigState from './state'

function reducer(state = twigState, action) {
    switch (action.type) {
        case 'LOAD-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs
            });
        case 'FIND-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs
            });
        case 'INSERT-TWIG':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'UPDATE-TWIG':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'REMOVE-TWIG':
            return Object.assign({}, state, {
                type: action.type
            });
        default:
            return state
    }
}

export default reducer
