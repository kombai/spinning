import Helper from '../../../common/helper';
const fs = electronRequire("fs-extra");
import dataStore from './store-data';

const TwigAction = {

    loadData(dispatch) {
        dataStore.find({} , function(err, docs) {
			dispatch({
                type: "LOAD-TWIG",
                twigs: docs
            });
		});
    },

    findData(dispatch, textSearch) {
        dataStore.find({name: new RegExp(textSearch, 'i')} , function(err, docs) {
            dispatch({
                type: "FIND-TWIG",
                twigs: docs
            });
		});
    },

    insert(dispatch, twig) {
        dataStore.insert(twig, function(err, newDoc) {
            dispatch({
                type: "INSERT-TWIG"
            });
        });
    },

    update(dispatch, twig) {
        dataStore.update({_id: twig._id }, {$set: twig}, {}, function (err) {
            dispatch({
                type: "UPDATE-TWIG"
            });
        });
    },

    remove(dispatch, twig) {
        dataStore.remove({_id: twig._id}, {}, function(err) {
            dispatch({
                type: "REMOVE-TWIG"
            });
        });
    }
}

export default TwigAction
