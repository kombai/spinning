let Datastore = electronRequire('nedb');

let twigData = new Datastore({
    filename: './storage/data-store/twig-data.db'
});

export default twigData;
