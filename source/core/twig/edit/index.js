import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';

class EditTwig extends Component {

    constructor(props, context) {
        super(props);

        this.state = {
			twig: {}
        }
    }

    componentDidMount() {
        let location = this.props.location;
        let twig = location.state;
        this.setState({
            twig: twig
        });
    }

	render() {
        return (
            <Planting
                mode="update"
                twig={this.state.twig}
            />
        );
	}
}

export default EditTwig
