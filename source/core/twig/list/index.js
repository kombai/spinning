import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import Helper from '../../../common/helper';
import TwigAction from '../connect/action';

class ListTwig extends Component {

    constructor(props, context) {
        super(props);

        this.state = {
            twigs: []
        }
        this.findTwig = this.findTwig.bind(this);
    }

    componentWillMount() {
		this.props.loadData();
	}

    componentWillReceiveProps(newProps) {
		if (newProps.type == "LOAD-TWIG" || newProps.type == "FIND-TWIG") {
			this.setState({
				twigs: newProps.twigs
			});
		} else {
            // when update twig;
			this.props.loadData();
		}
    }

    findTwig(event) {
		this.props.findData(event.target.value);
	}

	render() {
        return (
            <div className="list-twig page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <div className="pull-right">
                            <Link to={{pathname: "create-twig", state: null}}>
                                <button className="btn btn-success">Create a twig</button>
                            </Link>
                        </div>
                        <h2> List TWIG</h2>
                        <div className="form-group has-feedback">
                            <input type="text" className="form-control" placeholder="search" onChange={this.findTwig}/>
                            <i className="glyphicon glyphicon-search form-control-feedback"></i>
                        </div>
                    </div>
                    <div className="page-body">
                        <div className="list-content">
                        {
    						this.state.twigs.map(function(item, index) {
    							return (
                                    <div key={item._id} className="twig">
                                        <div className="thumbnail">
                                            <Link className="link" to={{pathname: "edit-twig/" + item._id, state: item}}>{item.name}</Link>
                                        </div>
                                    </div>
    							);
    						}.bind(this))
    					}
                        </div>
                    </div>
                </div>
                <div className="page-extend"></div>
            </div>
		);
	}
}

function mapStateToProps(fullState, thisProps) {
	return fullState.twig;
}

const mapAction = (dispatch) => {
	return {
        loadData: () => {
            TwigAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			TwigAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(ListTwig)
