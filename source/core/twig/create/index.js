import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';

class CreateTwig extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            twig: {}
        };
    }

    componentDidMount() {
        this.setState({
            twig: {
                _id: Helper.getGUID(),
                name: "New Twig",
                value: []
            }
        });
    }

	render() {
		return (
			<Planting
                mode="insert"
                twig={this.state.twig}
            />
		);
	}
}


export default CreateTwig
