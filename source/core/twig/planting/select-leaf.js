const electronRemote = electronRequire("electron").remote;
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';
import LeafAction from '../../leaf/connect/action';


class SelectLeaf extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			existLeaves: [],
			twigLeaves: [],
			visible: false,
			isEdit: false,

			leafIndex: -1,
			currentText: "",
			customImage: "",

			selectedLeaf: null
		};
		this.selectImage = this.selectImage.bind(this);
		this.removeImage = this.removeImage.bind(this);
		this.onTextChange = this.onTextChange.bind(this);

		this.clearLeafName = this.clearLeafName.bind(this);
		this.applyRemoveLeaf = this.applyRemoveLeaf.bind(this);
		this.applyUpdateLeaf = this.applyUpdateLeaf.bind(this);
		this.cancelUpdateLeaf = this.cancelUpdateLeaf.bind(this);
	}

	componentWillReceiveProps(newProps) {
		// only update when modal is visible;
		if (this.state.visible) {
			this.setState({
				existLeaves: newProps.leaves
			});
		}
	}

	componentWillMount() {
		this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
	}

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let twigLeaves = plantingState.leaves;
		let actionType = plantingState.type;
		let leafIndex = plantingState.leafIndex;
		let isEdit = leafIndex < 0 ? false : true;
		let selectedLeaf = twigLeaves[leafIndex];

		if (actionType == "SELECT-LEAF") {
			this.state.isEdit = isEdit;
			this.state.visible = true;

			if (selectedLeaf) {
				this.state.selectedLeaf = selectedLeaf;
				this.state.currentText = selectedLeaf.name;
			}
        } else {
			this.state.visible = false;
			this.state.existLeaves = [];
			this.state.currentText = "";
			this.state.customImage = "";
			this.state.selectedLeaf = null;
		}

		this.setState({
			leafIndex: leafIndex,
			twigLeaves: twigLeaves
		});
    }

	selectImage() {
		let showDialog = electronRemote.dialog.showOpenDialog;
        showDialog({
            filters: [
                {name: 'Images', extensions: ['jpg', 'png']}
            ]
        }, function (fileNames) {
            if (fileNames && fileNames.length) {
                this.setState({
					existLeaves: [],
					customImage: fileNames[0]
				});
            }
        }.bind(this));
	}

	removeImage() {
		this.setState({
			customImage: ""
		});
		this.props.findData(this.state.currentText);
	}

	onTextChange(event) {
		let value = event.target.value;
        this.setState({
            currentText: value,
			selectedLeaf: null
        });

		if (!this.state.customImage) {
			if (!value.length) {
				// load exist twig;
				this.props.loadData();
			} else {
				// search in exist twig;
				this.props.findData(value);
			}
		}
    }

	selectLeaf(selectedLeaf) {
		this.setState({
			currentText: selectedLeaf.name,
			selectedLeaf: selectedLeaf
		});
	}

	cancelUpdateLeaf() {
		this.setState({
			visible: false
		});
	}

	clearLeafName() {
		this.setState({
			currentText: ""
		});
		// load exist twig;
		this.props.loadData();
	}

	applyRemoveLeaf() {
		let twigLeaves = this.state.twigLeaves;
		let leafIndex = this.state.leafIndex;

		// only remove in edit mode;
		if (this.state.isEdit) {
			this.cancelUpdateLeaf();
			twigLeaves.splice(leafIndex, 1);
			TinyAction.updateTwig(twigLeaves);
		}
	}

	applyUpdateLeaf() {
		let twigLeaves = this.state.twigLeaves;
		let leafIndex = this.state.leafIndex;
		let leafObj = this.state.selectedLeaf;
		let leafName = this.state.currentText;

		// require the name for new leaf;
		if (leafName.trim() === "") return;

		// create new leaf;
		if (!leafObj) {
			let customImage = this.state.customImage;
			let leafId = Helper.getGUID();
			let leafType = "none";
			let leafValue = "";

			if (customImage) {
				leafType = "image";
				leafValue = leafId.replace(/-/g, "/") + ".png";
			} else {
				leafValue = leafName;
			}

		 	leafObj = {
				_id: leafId,
				name: leafName,
				type: leafType,
				value: leafValue
			};

			this.props.insert(leafObj, customImage);
		}

		// waiting for inserted new leaf;
		setTimeout(() => {
			if (leafIndex < 0) {
				// add new leaf;
				twigLeaves.push(leafObj);
			} else {
				// update leaf;
				twigLeaves[leafIndex] = leafObj;
			}
			TinyAction.updateTwig(twigLeaves);
		}, 345);
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let selectedLeaf = this.state.selectedLeaf;
		let currentText = this.state.currentText;
		let customStyle = null;
		if (this.state.customImage) {
			customStyle = {
				backgroundImage: "url(" + this.state.customImage + ")"
			};
		}
		let require = currentText ? "" : "require";
		let isEdit = this.state.isEdit;

		return (
			<div className="modal select-leaf">
				<div className="content">
					<div className="modal-header">
						<button className="btn btn-default pull-right" onClick={this.cancelUpdateLeaf}>Close</button>
						<button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateLeaf}>Apply</button>
						<h3 className="modal-title">{isEdit ? "Edit Leaf" : "Add Leaf"}</h3>
					</div>
					<div className="modal-body">
						<div className="form-group leaf-image" style={this.state.customImage ? customStyle : null}>
			                <div className="btn-group" >
								<button className="btn btn-success" onClick={this.selectImage}>Select photo</button>
								{
									this.state.customImage ?
										<button className="btn btn-danger" onClick={this.removeImage}><span className="glyphicon glyphicon-remove"></span></button>
										:
										null
								}
							</div>
					    </div>

						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearLeafName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveLeaf}></i>
										:
										null

							}
						</div>
					</div>
					<div className="list-leaf">
					{

						this.state.existLeaves.map(function(leaf, index) {
							let customStyle = null;
							if (leaf.type == "image") {
								customStyle = {
									backgroundImage: "url(" + Helper.getImagePath(leaf.value) + ")"
								};
							}
							let leafStyle = "leaf";
							if (selectedLeaf && selectedLeaf._id == leaf._id) {
								leafStyle = "leaf selected";
							}

							return (
								<div key={leaf._id} className={leafStyle} onClick={() => this.selectLeaf(leaf)}>
									<div className="thumbnail">
										{ customStyle ? <div className="photo" style={customStyle}></div> : null}
										<div className="caption">{leaf.name}</div>
									</div>
								</div>
							);
						}.bind(this))
					}
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(fullState, thisProps) {
	return fullState.leaf;
}

const mapAction = (dispatch) => {
	return {
		insert: (leaf, imagePath) => {
            LeafAction.insert(dispatch, leaf, imagePath);
        },
        loadData: () => {
            LeafAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			LeafAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(SelectLeaf);
