import React, {Component, PropTypes} from 'react';
import TinyAction from './tiny-action';
import TinyStore from './tiny-store';

class SelectTwig extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			visible: false,
			currentText: ""
		};

		this.onTextChange = this.onTextChange.bind(this);

		this.clearTwigName = this.clearTwigName.bind(this);
		this.applyRemoveTwig = this.applyRemoveTwig.bind(this);
		this.applyUpdateTwig = this.applyUpdateTwig.bind(this);
		this.cancelSelectTwig = this.cancelSelectTwig.bind(this);
	}

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let actionType = plantingState.type;

		if (actionType == "SELECT-TWIG") {
			this.state.visible = true;
        } else {
			this.state.visible = false;
		}

		this.setState({
			currentText: plantingState.name
		});
    }

	onTextChange(event) {
		this.setState({
            currentText: event.target.value
        });
	}

	cancelSelectTwig() {
		this.setState({
			visible: false
		});
	}

	clearTwigName() {
		this.setState({
			currentText: ""
		});
	}

	applyUpdateTwig() {
		let name = this.state.currentText;
		if (name.trim() != "") {
			TinyAction.updateName(name);
		}
	}

	applyRemoveTwig() {
		let name = this.state.currentText;
		if (name.trim() == "") {
			this.props.removeTwig();
		}
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let currentText = this.state.currentText;
		let require = currentText ? "" : "require";

		let isEdit = false;
		if (this.props.mode === "update") {
			isEdit = true;
		}

		return (
			<div className="modal select-twig">
				<div className="content">
					<div className="modal-header">
			            <button className="btn btn-default pull-right" onClick={this.cancelSelectTwig}>Close</button>
			            <button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateTwig}>Apply</button>
			            <h3 className="modal-title">{isEdit ? "Edit Twig" : "Create Twig"}</h3>
			        </div>
					<div className="modal-body">
						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearTwigName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveTwig}></i>
										:
										null
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SelectTwig.contextTypes = {
    router: PropTypes.object.isRequired
}

SelectTwig.propTypes = {
	mode: PropTypes.string,
    removeTwig: PropTypes.func
}

export default SelectTwig;
