import {createStore} from 'redux';

let tinyState = {
    type: "",
    name: "",
    leaves: [],
    leafIndex: -1
};

function reducer(state = tinyState, action) {
    switch (action.type) {
        case 'SELECT-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                leaves: action.leaves,
                leafIndex: action.leafIndex
            });

        case 'SELECT-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                name: action.name
            });

        case 'UPDATE-NAME':
            return Object.assign({}, state, {
                type: action.type,
                name: action.name
            });

        case 'UPDATE-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                leaves: action.leaves
            });
        default:
            return state
    }
}

export default createStore(reducer)
