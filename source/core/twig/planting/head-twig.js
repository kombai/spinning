import React, {Component, PropTypes} from 'react';
import TinyAction from './tiny-action';
import TinyStore from './tiny-store';

class HeadTwig extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            name: ""
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            name: newProps.name
        });
    }

    componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
        let actionType = plantingState.type;
        let name = plantingState.name;

        if (actionType == "UPDATE-NAME") {
            this.setState({
                name: name
            });
            this.props.setTwigData('name', name);
        }
    }

    selectTwig() {
        TinyAction.selectTwig(this.state.name);
    }

	render() {
		return (
            <section onClick={() => this.selectTwig()}>
                <div className="pull-right add-new">
                    <span className="big-text glyphicon glyphicon-pencil"></span>
                    <span>...</span>
                </div>
    			<h2>
                    {this.state.name}
    		    </h2>
            </section>
		);
	}
}


HeadTwig.contextTypes = {
    router: PropTypes.object.isRequired
};

HeadTwig.propTypes = {
	name: PropTypes.string,
    setTwigData: PropTypes.func
};

export default HeadTwig
