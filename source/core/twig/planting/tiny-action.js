import Helper from '../../../common/helper';
import TinyStore from './tiny-store';

const TinyAction = {

    selectTwig(name) {
        TinyStore.dispatch({
            type: "SELECT-TWIG",
            name: name
        });
    },

    updateTwig(leaves) {
        TinyStore.dispatch({
            type: "UPDATE-TWIG",
            leaves: leaves
        });
    },

    updateName(name) {
        TinyStore.dispatch({
            type: "UPDATE-NAME",
            name: name
        });
    },

    selectLeaf(leaves, leafIndex) {
        TinyStore.dispatch({
            type: "SELECT-LEAF",
            leaves: leaves,
            leafIndex: leafIndex
        });
    }
}

export default TinyAction;
