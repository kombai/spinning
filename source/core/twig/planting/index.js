import React, {Component, PropTypes} from 'react';
import TwigAction from '../connect/action';
import {connect} from 'react-redux';
import Helper from '../../../common/helper';

import HeadTwig from "./head-twig";
import ListLeaf from "./list-leaf";
import SelectLeaf from './select-leaf';
import SelectTwig from './select-twig';

class Planting extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            twigObj: {},
            mode: "", // insert, update
            id: "",
            name: "",
            leaves: [] // twig value
        };

        //binding event;
        this.saveTwig = this.saveTwig.bind(this);
        this.saveAsNew = this.saveAsNew.bind(this);
        this.removeTwig = this.removeTwig.bind(this);
        this.setTwigData = this.setTwigData.bind(this);
    }

    componentWillReceiveProps(newProps) {
        let twig = newProps.twig;
        let mode = newProps.mode;
        this.setState({
            twigObj: twig,
            mode: mode,
            id: twig._id,
            name: twig.name,
            leaves: twig.value
        });
    }

    saveTwig() {
        let leaves = this.state.leaves;
        let name = this.state.name;
        let id = this.state.id;
        let mode = this.state.mode;

        if (!name) {
            alert("The name of twig is require !");
            return;
        }

        let twigObj = {
            _id: id,
            name: name,
            value: leaves
        };

        if (mode === "insert") {
            this.props.insert(twigObj);
            // change mode to update;
            this.setState({
                mode: "update"
            });
        } else if (mode === "update") {
            this.props.update(twigObj);
        }

        setTimeout(() => {
            this.context.router.replace("/list-twig");
        }, 345);
    }

    saveAsNew() {
        let twigObj = {
            _id: Helper.getGUID(),
            name: this.state.name,
            value: this.state.leaves
        };
        this.props.insert(twigObj);

        setTimeout(() => {
            this.context.router.replace("/list-twig");
        }, 345);
    }

    removeTwig() {
        // only remove when action mode is update;
        if (this.state.mode == "update") {
            this.props.remove(this.state.twigObj);
            setTimeout(() => {
                this.context.router.replace("/list-twig");
            }, 345);
        }
    }

    setTwigData(propType, propData) {
        this.state[propType] = propData;
    }

	render() {
        let twigs = this.state.twigs;
		return (
			<div className="twig-page page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <HeadTwig
                            name={this.state.name}
                            setTwigData={this.setTwigData}
                        ></HeadTwig>
                    </div>
                    <div className="page-body">
                        <ListLeaf
                            leaves={this.state.leaves}
                            setTwigData={this.setTwigData}
                        ></ListLeaf>
                    </div>

                    <div className="page-footer">
                        <button className="btn btn-lg btn-success" onClick={this.saveTwig}>Save</button>
                        <span> </span>
                        {
                            this.state.mode === "update" ?
                                <button className="btn btn-lg btn-info" onClick={this.saveAsNew}>Save as New</button>
                                :
                                null
                        }
                        <span> </span>
                        <button className="btn btn-lg btn-warning pull-right" onClick={this.context.router.goBack}>Cancel</button>
    				</div>

                </div>
                <div className="page-extend">
                    <SelectLeaf />

                    <SelectTwig
                        mode={this.state.mode}
                        removeTwig={this.removeTwig}
                    ></SelectTwig>
                </div>
		    </div>
		);``
	}
}


Planting.contextTypes = {
    router: PropTypes.object.isRequired
}

const mapAction = (dispatch) => {
	return {
        insert: (twig) => {
            TwigAction.insert(dispatch, twig);
        },
        update: (twig) => {
            TwigAction.update(dispatch, twig);
        },
        remove: (twig) => {
            TwigAction.remove(dispatch, twig);
        }
  	}
}

export default connect(null, mapAction)(Planting)
