import React, {Component, PropTypes} from 'react';
import TinyAction from './tiny-action';
import TinyStore from './tiny-store';
import Helper from '../../../common/helper';

class ListLeaf extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            leaves: props.leaves
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            leaves: newProps.leaves
        });
    }

    componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
        let leaves = plantingState.leaves;
        let type = plantingState.type;

        if (type == "UPDATE-TWIG") {
            this.setState({
                leaves: leaves
            });
            // update plant value;
            this.props.setTwigData("leaves", leaves);
        }
    }

    selectLeaf(index) {
        let leaves = this.state.leaves;
        TinyAction.selectLeaf(leaves, index);
    }

	render() {
        let leaves = this.state.leaves;

		return (
            <div className="panel panel-success">
                <div className="panel-heading" onClick={() => this.selectLeaf(-1)}>
                    <div className="add-new">
                        <span className="glyphicon glyphicon-leaf"></span>+
                    </div>
                </div>
                <ul className="list-group">
    			{
                    leaves.length ? leaves.map(function(leaf, leafIndex) {
                        let customeStyle = null;
                        let leafContent = null;
                        if (leaf.type === "image") {
                            customeStyle = {
                                backgroundImage: "url(" + Helper.getImagePath(leaf.value) + ")"
                            };
                            leafContent = (<div className="photo" style={customeStyle}></div>);
                        } else if (leaf.type === "none") {
                            leafContent = leaf.value;
                        }

                        return (
                            <li className="list-group-item" key={leafIndex} onClick={() => this.selectLeaf(leafIndex)}>
                                {leafContent}
                            </li>
                        );
                    }.bind(this)) : null
    			}
                </ul>
            </div>
		);
	}
}

ListLeaf.contextTypes = {
    router: PropTypes.object.isRequired
};

ListLeaf.propTypes = {
	leaves: PropTypes.array,
    setTwigData: PropTypes.func
};

export default ListLeaf
