import React, {Component, PropTypes} from 'react';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';

class HeadTree extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            treeName: ""
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            treeName: newProps.treeName
        });
    }

    componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
        let treeName = plantingState.treeName;
        let type = plantingState.type;

        if (type === "UPDATE-TREE") {
            this.setState({
                treeName: treeName
            });
            this.props.setTreeData('treeName', treeName);
        }
    }

    selectTree() {
        TinyAction.selectTree(this.state.treeName);
    }

	render() {
		return (
            <section onClick={() => this.selectTree()}>
                <div className="pull-right add-new">
                    <span className="big-text glyphicon glyphicon-pencil"></span>
                    <span>...</span>
                </div>
    			<h2>
                    {this.state.treeName}
    		    </h2>
            </section>
		);
	}
}


HeadTree.contextTypes = {
    router: PropTypes.object.isRequired
}

HeadTree.propTypes = {
    treeName: PropTypes.string,
    setTreeData: PropTypes.func
}


export default HeadTree;
