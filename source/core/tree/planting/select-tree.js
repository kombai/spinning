import React, {Component, PropTypes} from 'react';
import TinyAction from './tiny-action';
import TinyStore from './tiny-store';

class SelectTree extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			visible: false,
			currentText: ""
		};

		this.onTextChange = this.onTextChange.bind(this);

		this.clearTreeName = this.clearTreeName.bind(this);
		this.applyUpdateTree = this.applyUpdateTree.bind(this);
		this.applyRemoveTree = this.applyRemoveTree.bind(this);
		this.cancelSelectTree = this.cancelSelectTree.bind(this);
	}

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let type = plantingState.type;

		if (type == "SELECT-TREE") {
			this.state.visible = true;
        } else {
			this.state.visible = false;
		}

		this.setState({
			currentText: plantingState.treeName
		});
    }

	onTextChange(event) {
		this.setState({
            currentText: event.target.value
        });
	}

	cancelSelectTree() {
		this.setState({
			visible: false
		});
	}

	clearTreeName() {
		this.setState({
			currentText: ""
		});
	}

	applyRemoveTree() {
		let treeName = this.state.currentText;
		if (treeName.trim() == "") {
			this.props.removeTree();
		}
	}

	applyUpdateTree() {
		let treeName = this.state.currentText;
		if (treeName.trim() != "") {
			TinyAction.updateTree(treeName);
		}
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let currentText = this.state.currentText;
		let require = currentText ? "" : "require";

		let isEdit = false;
		if (this.props.mode === "update") {
			isEdit = true;
		}

		return (
			<div className="modal select-tree">
				<div className="content">
					<div className="modal-header">
			            <button className="btn btn-default pull-right" onClick={this.cancelSelectTree}>Close</button>
			            <button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateTree}>Apply</button>
			            <h3 className="modal-title">{isEdit ? "Edit Tree" : "Create Tree"}</h3>
			        </div>
					<div className="modal-body">
						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearTreeName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveTree}></i>
										:
										null
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

SelectTree.contextTypes = {
    router: PropTypes.object.isRequired
}

SelectTree.propTypes = {
	mode: PropTypes.string,
    removeTree: PropTypes.func
}

export default SelectTree;
