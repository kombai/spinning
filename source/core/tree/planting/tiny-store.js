import {createStore} from 'redux';

let tinyState = {
    type: "",
    twigs: [],
    propType: "", // long term, short term, temporary
    treeName: "",
    leafIndex: -1,
    twigIndex: -1
}

function reducer(state = tinyState, action) {
    switch (action.type) {
        case 'SELECT-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs,
                propType: action.propType,
                twigIndex: action.twigIndex
            });

        case 'UPDATE-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs,
                propType: action.propType
            });

        case 'SELECT-LEAF':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs,
                propType: action.propType,
                leafIndex: action.leafIndex,
                twigIndex: action.twigIndex
            });

        case 'SELECT-TREE':
            return Object.assign({}, state, {
                type: action.type,
                treeName: action.treeName
            });

        case 'UPDATE-TREE':
            return Object.assign({}, state, {
                type: action.type,
                treeName: action.treeName
            });

        default:
            return state
    }
}

export default createStore(reducer)
