
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';

const TinyAction = {

    selectTwig(twigs, twigIndex, propType) {
        TinyStore.dispatch({
            type: "SELECT-TWIG",
            twigs: twigs,
            propType: propType,  // long term, short term, temporary
            twigIndex: twigIndex
        });
    },

    updateTwig(twigs, propType) {
        TinyStore.dispatch({
            type: "UPDATE-TWIG",
            twigs: twigs,
            propType: propType
        });
    },

    selectLeaf(twigs, twigIndex, leafIndex, propType) {
        TinyStore.dispatch({
            type: "SELECT-LEAF",
            twigs: twigs,
            propType: propType,  // long term, short term, temporary
            leafIndex: leafIndex,
            twigIndex: twigIndex
        });
    },

    selectTree(treeName) {
        TinyStore.dispatch({
            type: "SELECT-TREE",
            treeName: treeName
        });
    },

    updateTree(treeName) {
        TinyStore.dispatch({
            type: "UPDATE-TREE",
            treeName: treeName
        });
    }

}

export default TinyAction;
