import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

import HeadTree from './head-tree';
import ListTwig from './list-twig';

import SelectLeaf from './select-leaf';
import SelectTwig from './select-twig';
import SelectTree from './select-tree';
import TreeAction from '../connect/action';

class Planting extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            temporary: [],
            shortTerm: [],
            longTerm: [],
            treeObj: {},
            mode: "",
            treeName: ""
        };
        this.preview = this.preview.bind(this);

        this.saveTree = this.saveTree.bind(this);
        this.removeTree = this.removeTree.bind(this);
        this.setTreeData = this.setTreeData.bind(this);
    }

	componentWillReceiveProps(newProps) {
        let tree = newProps.tree;
        this.setState({
            treeName: tree.name,
            treeObj: tree,
            mode: newProps.mode,
            longTerm: tree.value.longTerm,
            shortTerm: tree.value.shortTerm,
            temporary: tree.value.temporary
        });
    }

    removeTree() {
        let mode = this.state.mode;
        if (mode == "update") {
            this.props.remove(this.state.treeObj);
        }
    }

    saveTree() {
        let mode = this.state.mode;
        let treeObj = {
            _id: this.state.treeObj._id,
            name: this.state.treeName,
            value: {
                longTerm: this.state.longTerm,
                shortTerm: this.state.shortTerm,
                temporary: this.state.temporary
            }
        };

        if (mode === "insert") {
            this.props.insert(treeObj);
        } else {
            this.props.update(treeObj);
        }
        console.log("treeObj", treeObj);

        setTimeout(() => {
            this.context.router.goBack();
        }, 345);
    }

    preview() {
        let treeObj = {
            _id: this.state.treeObj._id,
            name: this.state.treeName,
            value: {
                longTerm: this.state.longTerm,
                shortTerm: this.state.shortTerm,
                temporary: this.state.temporary
            }
        };
        this.context.router.push({
            state: treeObj,
	        pathname: '/preview-result'
        });
    }

    setTreeData(dataKey, dataValue) {
        this.state[dataKey] = dataValue;
    }

	render() {
		return (
			<div className="planting-page page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <HeadTree
                            treeName={this.state.treeName}
                            setTreeData={this.setTreeData}
                        ></HeadTree>
                    </div>
                    <div className="page-body">
                        <ListTwig
                            title="Long Term"
                            term="longTerm"
                            twigs={this.state.longTerm}
                            setTreeData={this.setTreeData}
                        ></ListTwig>

                        <ListTwig
                            title="Short Term"
                            term="shortTerm"
                            twigs={this.state.shortTerm}
                            setTreeData={this.setTreeData}
                        ></ListTwig>

                        <ListTwig
                            title="Temporary"
                            term="temporary"
                            twigs={this.state.temporary}
                            setTreeData={this.setTreeData}
                        ></ListTwig>
                    </div>

    				<div className="page-footer">
                        <button className="btn btn-lg btn-success" onClick={this.saveTree}>Save</button>
                        <button className="btn btn-lg btn-info" onClick={this.preview}>Preview</button>
                        <button className="btn btn-lg btn-warning pull-right" onClick={this.context.router.goBack}>Cancel</button>
    				</div>
                </div>

                <div className="page-extend">
                    <SelectLeaf />
                    <SelectTwig />
                    <SelectTree
                        mode={this.state.mode}
                        removeTree={this.removeTree}
                    ></SelectTree>
                </div>
		    </div>
		);
	}
}


Planting.contextTypes = {
    router: PropTypes.object.isRequired
}

const mapAction = (dispatch) => {
	return {
        insert: (tree) => {
            TreeAction.insert(dispatch, tree);
        },
        update: (tree) => {
            TreeAction.update(dispatch, tree);
        },
        remove: (tree) => {
            TreeAction.remove(dispatch, tree);
        }
  	}
}

export default connect(null, mapAction)(Planting)
