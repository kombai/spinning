import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';
import TwigAction from "../../twig/connect/action";

class SelectTwig extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			existTwigs: [], // twig from db
			treeTwigs: [], // the twig of tree
			propType: "",  // long term, short term, temporary
			isEdit: false,
			visible: false,

			twigIndex: -1,
			currentText: "",
			selectedTwig: null
		};
		this.onTextChange = this.onTextChange.bind(this);

		this.clearTwigName = this.clearTwigName.bind(this);
		this.applyRemoveTwig = this.applyRemoveTwig.bind(this);
		this.applyUpdateTwig = this.applyUpdateTwig.bind(this);
		this.cancelUpdateTwig = this.cancelUpdateTwig.bind(this);
	}

	componentWillReceiveProps(newProps) {
		this.setState({
			existTwigs: newProps.twigs
		});
	}

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let twigIndex = plantingState.twigIndex;
		let propType = plantingState.propType;
		let treeTwigs = plantingState.twigs;
		let actionType = plantingState.type;

		let isEdit = twigIndex < 0 ? false : true;
		let selectedTwig = treeTwigs[twigIndex];

		if (actionType == "SELECT-TWIG") {
			this.state.isEdit = isEdit;
			this.state.visible = true;

			if (selectedTwig) {
				this.state.selectedTwig = selectedTwig;
				this.state.currentText = selectedTwig.name;
			}

			// load exist twig;
			this.props.loadData();
        } else {
			this.resetState();
			this.state.visible = false;
		}

		this.setState({
			propType: propType,
			twigIndex: twigIndex,
			treeTwigs: treeTwigs
		});
    }

	onTextChange(event) {
		let value = event.target.value;
		this.setState({
            currentText: value,
			selectedTwig: null
        });

		if (!value.length) {
			// load exist twig;
			this.props.loadData();
		} else {
			// search in exist twig;
			this.props.findData(value);
		}
	}

	selectTwig(selectedTwig) {
		this.setState({
			currentText: selectedTwig.name,
			selectedTwig: selectedTwig
		});
	}

	resetState() {
		this.state.existTwigs = [];
		this.state.currentText = "";
		this.state.twigIndex = -1;
		this.state.selectedTwig = null;
	}

	cancelUpdateTwig() {
		this.resetState();
		this.setState({
			visible: false
		});
	}

	clearTwigName() {
		this.setState({
			currentText: ""
		});
		// load exist twig;
		this.props.loadData();
	}

	applyRemoveTwig() {
		let treeTwigs = this.state.treeTwigs;
		let twigIndex = this.state.twigIndex;
		let propType = this.state.propType;

		// only remove in edit mode;
		if (this.state.isEdit) {
			this.cancelUpdateTwig();
			treeTwigs.splice(twigIndex, 1);
			TinyAction.updateTwig(treeTwigs, propType);
		}
	}

	applyUpdateTwig() {
		let selectedTwig = this.state.selectedTwig;
		let currentText = this.state.currentText;
		let twigIndex = this.state.twigIndex;
		let treeTwigs = this.state.treeTwigs;
		let propType = this.state.propType;

		// require the name for new twig;
		if (currentText.trim() === "") return;

		if (this.state.isEdit) {

			if (selectedTwig) {
				treeTwigs[twigIndex] = selectedTwig;
			} else {
				// update name only;
				treeTwigs[twigIndex].name = currentText;
			}
		} else {

			if (selectedTwig) {
				treeTwigs.push(selectedTwig);
			} else {
				treeTwigs.push({
					name: currentText,
					value: []
				});
			}
		}
		// require the name for new twig;
		TinyAction.updateTwig(treeTwigs, propType);
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let selectedTwig = this.state.selectedTwig;
		let currentText = this.state.currentText;
		let require = currentText ? "" : "require";
		let isEdit = this.state.isEdit;

		return (
			<div className="modal select-twig">
				<div className="content">
					<div className="modal-header">
						<button className="btn btn-default pull-right" onClick={this.cancelUpdateTwig}>Close</button>
						<button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateTwig}>Apply</button>
						<h3 className="modal-title">{isEdit ? "Edit Twig" : "Add Twig"}</h3>
					</div>
					<div className="modal-body">
						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearTwigName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveTwig}></i>
										:
										null

							}
						</div>
					</div>
					<div className="list-twig">
					{
						this.state.existTwigs.map(function(twig, index) {
							let twigStyle = "twig";
							if (selectedTwig && selectedTwig._id == twig._id) {
								twigStyle = "twig selected";
							}
							return (
								<div key={twig._id} className={twigStyle} onClick={() => this.selectTwig(twig)}>
									<div className="thumbnail">
										<div className="caption">{twig.name}</div>
									</div>
								</div>
							);
						}.bind(this))
					}
					</div>
				</div>
			</div>
		);
	}
}


function mapStateToProps(fullState, thisProps) {
	return fullState.twig;
}

const mapAction = (dispatch) => {
	return {
        loadData: () => {
            TwigAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			TwigAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(SelectTwig);
