const electronRemote = electronRequire("electron").remote;
import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';
import LeafAction from '../../leaf/connect/action';


class SelectLeaf extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			existLeaves: [],
			treeTwigs: [],
			propType: "",  // long term, short term, temporary
			isEdit: false,
			visible: false,

			twigIndex: -1,
			leafIndex: -1,
			currentText: "",
			customImage: "",

			selectedLeaf: null
		};
		this.selectImage = this.selectImage.bind(this);
		this.removeImage = this.removeImage.bind(this);
		this.onTextChange = this.onTextChange.bind(this);

		this.clearLeafName = this.clearLeafName.bind(this);
		this.applyRemoveLeaf = this.applyRemoveLeaf.bind(this);
		this.applyUpdateLeaf = this.applyUpdateLeaf.bind(this);
		this.cancelUpdateLeaf = this.cancelUpdateLeaf.bind(this);
	}

	componentWillReceiveProps(newProps) {
		this.setState({
			existLeaves: newProps.leaves
		});
	}

	componentWillMount() {
		this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
	}

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
		let plantingState = TinyStore.getState();
		let twigIndex = plantingState.twigIndex;
		let leafIndex = plantingState.leafIndex;
        let propType = plantingState.propType;
		let treeTwigs = plantingState.twigs;
		let actionType = plantingState.type;

		if (actionType == "SELECT-LEAF") {
			let selectedLeaf = treeTwigs[twigIndex].value[leafIndex];
			this.state.isEdit = leafIndex < 0 ? false : true;
			this.state.visible = true;

			if (selectedLeaf) {
				this.state.selectedLeaf = selectedLeaf;
				this.state.currentText = selectedLeaf.name;
			}

			if (!this.state.isEdit) {
				// load exist leaf;
				this.props.loadData();
			}
        } else {
			this.resetState();
			this.state.visible = false;
		}

		this.setState({
			propType: propType,
			treeTwigs: treeTwigs,
			twigIndex: twigIndex,
			leafIndex: leafIndex
		});

    }

	selectImage() {
		let showDialog = electronRemote.dialog.showOpenDialog;
        showDialog({
            filters: [
                {name: 'Images', extensions: ['jpg', 'png']}
            ]
        }, function (fileNames) {
            if (fileNames && fileNames.length) {
                this.setState({
					selectedLeaf: null,
					existLeaves: [],
					customImage: fileNames[0]
				});
            }
        }.bind(this));
	}

	removeImage() {
		this.setState({
			customImage: ""
		});
		this.props.findData(this.state.currentText);
	}

	onTextChange(event) {
		let value = event.target.value;
        this.setState({
            currentText: value,
			selectedLeaf: null
        });

		if (!this.state.customImage) {
			if (!value.length) {
				// load exist twig;
				this.props.loadData();
			} else {
				// search in exist twig;
				this.props.findData(value);
			}
		}
    }

	selectLeaf(selectedLeaf) {
		this.setState({
			currentText: selectedLeaf.name,
			selectedLeaf: selectedLeaf
		});
	}

	resetState() {
		this.state.existLeaves = [];
		this.state.currentText = "";
		this.state.customImage = "";
		this.state.twigIndex = -1;
		this.state.leafIndex = -1;
		this.state.selectedLeaf = null;
	}

	cancelUpdateLeaf() {
		this.resetState();
		this.setState({
			visible: false
		});
	}

	clearLeafName() {
		this.setState({
			currentText: ""
		});
		// load exist twig;
		this.props.loadData();
	}

	applyRemoveLeaf() {
		let treeTwigs = this.state.treeTwigs;
		let propType = this.state.propType;

		let twigIndex = this.state.twigIndex;
		let leafIndex = this.state.leafIndex;
		
		// only remove in edit mode;
		if (this.state.isEdit) {
			let leaf = treeTwigs[twigIndex].value;
			leaf.splice(leafIndex, 1);
			this.cancelUpdateLeaf();
			TinyAction.updateTwig(treeTwigs, propType);
		}
	}

	applyUpdateLeaf() {
		let treeTwigs = this.state.treeTwigs;
		let propType = this.state.propType;

		let twigIndex = this.state.twigIndex;
		let leafIndex = this.state.leafIndex;

		let leafObj = this.state.selectedLeaf;
		let leafName = this.state.currentText;

		// require the name for new leaf;
		if (leafName.trim() === "") return;

		// create new leaf;
		if (!leafObj) {
			let customImage = this.state.customImage;
			let leafId = Helper.getGUID();
			let leafType = "none";
			let leafValue = "";

			if (customImage) {
				leafType = "image";
				leafValue = leafId.replace(/-/g, "/") + ".png";
			} else {
				leafValue = leafName;
			}

		 	leafObj = {
				_id: leafId,
				name: leafName,
				type: leafType,
				value: leafValue
			};

			this.props.insert(leafObj, customImage);
		}

		// waiting for inserted new leaf;
		setTimeout(() => {
			if (!this.state.isEdit) {
				// add new leaf;
				treeTwigs[twigIndex].value.push(leafObj);
			} else {
				// update leaf;
				treeTwigs[twigIndex].value[leafIndex] = leafObj;
			}

			TinyAction.updateTwig(treeTwigs, propType);
		}, 345);
	}

	render() {
		if (this.state.visible == false) {
			return null;
		}

		let selectedLeaf = this.state.selectedLeaf;
		let currentText = this.state.currentText;
		let customStyle = null;
		if (this.state.customImage) {
			customStyle = {
				backgroundImage: "url(" + this.state.customImage + ")"
			};
		}
		let require = currentText ? "" : "require";
		let isEdit = this.state.isEdit;

		return (
			<div className="modal select-leaf">
				<div className="content">
					<div className="modal-header">
						<button className="btn btn-default pull-right" onClick={this.cancelUpdateLeaf}>Close</button>
						<button className={"btn btn-primary pull-left" + (currentText ? "" : " disabled")} onClick={this.applyUpdateLeaf}>Apply</button>
						<h3 className="modal-title">{isEdit ? "Edit Leaf" : "Add Leaf"}</h3>
					</div>
					<div className="modal-body">
						<div className="form-group leaf-image" style={this.state.customImage ? customStyle : null}>
			                <div className="btn-group" >
								<button className="btn btn-success" onClick={this.selectImage}>Select photo</button>
								{
									this.state.customImage ?
										<button className="btn btn-danger" onClick={this.removeImage}><span className="glyphicon glyphicon-remove"></span></button>
										:
										null
								}
							</div>
					    </div>

						<div className="has-feedback">
							<input type="text"
								className={"form-control " + require}
								value={currentText}
								onChange={this.onTextChange}
								placeholder={"Enter a new name" + (isEdit ? " or remove it !" : "")}
							/>
							{
								currentText ?
									<i className="glyphicon glyphicon-remove form-control-feedback" onClick={this.clearLeafName}></i>
									:
									isEdit ?
										<i className="glyphicon glyphicon-trash form-control-feedback" onClick={this.applyRemoveLeaf}></i>
										:
										null

							}
						</div>
					</div>
					<div className="list-leaf">
					{

						this.state.existLeaves.map(function(leaf, index) {
							let customStyle = null;
							if (leaf.type == "image") {
								customStyle = {
									backgroundImage: "url(" + Helper.getImagePath(leaf.value) + ")"
								};
							}
							let leafStyle = "leaf";
							if (selectedLeaf && selectedLeaf._id == leaf._id) {
								leafStyle = "leaf selected";
							}

							return (
								<div key={leaf._id} className={leafStyle} onClick={() => this.selectLeaf(leaf)}>
									<div className="thumbnail">
										{ customStyle ? <div className="photo" style={customStyle}></div> : null}
										<div className="caption">{leaf.name}</div>
									</div>
								</div>
							);
						}.bind(this))
					}
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(fullState, thisProps) {
	return fullState.leaf;
}

const mapAction = (dispatch) => {
	return {
		insert: (leaf, imagePath) => {
            LeafAction.insert(dispatch, leaf, imagePath);
        },
        loadData: () => {
            LeafAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			LeafAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(SelectLeaf);
