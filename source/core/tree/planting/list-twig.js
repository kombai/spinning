import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';

class ListTwig extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            title: "",
            term: "",
            twigs: []
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            title: newProps.title,
            term: newProps.term,
            twigs: newProps.twigs
        });
    }

    componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
        let actionType = plantingState.type;
        let propType = plantingState.propType;
        let twigs = plantingState.twigs;
        let term = this.state.term;

        if (propType !== term) return;

        if (actionType == "UPDATE-TWIG") {
            this.setState({
                twigs: twigs
            });
            // update term;
            this.props.setTreeData(term, twigs);
        }
    }

    selectTwig(index) {
        let twigs = this.state.twigs;
        let term = this.state.term;
        TinyAction.selectTwig(twigs, index, term);
    }

    selectLeaf(index, leaf) {
        let twigs = this.state.twigs;
        let term = this.state.term;
        TinyAction.selectLeaf(twigs, index, leaf, term);
    }

	render() {
        let twigs = this.state.twigs;
        let term = this.state.term;
        let panelColor = "";

        if (term === "longTerm") {
            panelColor = "panel panel-success";
        } else {
            panelColor = "panel panel-warning";
        }

		return (
            <div className="list-twig">
                <div className="heading">
                    <div className="pull-right add-new" onClick={() => this.selectTwig(-1)}>
                        <span className="glyphicon glyphicon-plus"></span>
                        <span className="glyphicon glyphicon-grain big-text"></span>
                    </div>
                    <div className="big-text">{this.state.title}</div>
                </div>

    			{
    				twigs.length ? twigs.map(function(twig, twigIndex) {
                        let leaves = twig.value;
    					return (
    						<div key={twigIndex} className={panelColor}>

    							<div className="panel-heading">
                                    <div className="pull-right add-new" onClick={() => this.selectLeaf(twigIndex, -1)}>
                                        <span className="glyphicon glyphicon-leaf"></span>+
                                    </div>
                                    <div className="twig-name" onClick={() => this.selectTwig(twigIndex)}>
                                        {twig.name}
                                    </div>
                                </div>
                                <ul className="list-group">
                                {
                                    leaves.length ? leaves.map(function(leaf, leafIndex) {
                                        let customeStyle = null;
                                        let leafContent = null;
                                        if (leaf.type === "image") {
                                            customeStyle = {
                    							backgroundImage: "url(" + Helper.getImagePath(leaf.value) + ")"
                    						};
                                            leafContent = (<div className="photo" style={customeStyle}></div>);
                                        } else if (leaf.type === "none") {
                                            leafContent = leaf.value;
                                        }

                                        return (
                                            <li className="list-group-item" key={twigIndex + "-" + leafIndex} onClick={() => this.selectLeaf(twigIndex, leafIndex)}>
                                                {leafContent}
                                            </li>
                                        );
                                    }.bind(this)) : null
                                }
                                </ul>
    						</div>
    					);
    				}.bind(this)) : null
    			}
            </div>
		);
	}
}


ListTwig.contextTypes = {
    router: PropTypes.object.isRequired
}

ListTwig.propTypes = {
    title: PropTypes.string,
    term: PropTypes.string,
    twigs: PropTypes.array,
    setTreeData: PropTypes.func
}

export default ListTwig;
