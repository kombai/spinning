import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';

class EditTree extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
			tree: {}
        };
    }

    componentDidMount() {
        let location = this.props.location;
        this.setPlant(location.state);
    }

    setPlant(treeData) {
        this.setState({
            tree: treeData
        });
    }

	render() {
        return (
			<Planting
                mode="update"
                tree={this.state.tree}
            />
		);
	}
}

export default EditTree
