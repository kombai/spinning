import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import TreeAction from '../connect/action';

class ListTree extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            trees: []
        };
        this.findTree = this.findTree.bind(this);
    }

    componentWillMount() {
		this.props.loadData();
	}

    componentWillReceiveProps(newProps) {
        if (newProps.type == "LOAD-TREE" || newProps.type == "FIND-TREE") {
			this.setState({
				trees: newProps.trees
			});
		} else {
			this.props.loadData();
		}
    }

    findTree(event) {
		this.props.findData(event.target.value);
	}

	render() {
        return (
            <div className="tree-list page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <div className="pull-right">
                            <Link to={{pathname: "create-tree", state: null}}>
                                <button className="btn btn-success">Create a tree</button>
                            </Link>
                        </div>
                        <h2>List TREE</h2>
    				    <div className="form-group has-feedback">
    				    	<input type="text" className="form-control" placeholder="search" onChange={this.findTree}/>
    						<i className="glyphicon glyphicon-search form-control-feedback"></i>
    				    </div>
                    </div>
                    <div className="page-body">
                    {
						this.state.trees.map(function(item, index) {
							return (
                                <div className="link" key={item._id}>
                                    <Link to={{pathname: "edit-tree/" + item._id, state: item}}>{item.name}</Link>
                                </div>
							);
						}.bind(this))
					}
                    </div>
                </div>
                <div className="page-extend"></div>
            </div>
		);
	}
}

function mapStateToProps(fullState, thisProps) {
	return fullState.tree;
}

const mapAction = (dispatch) => {
	return {
        loadData: () => {
            TreeAction.loadData(dispatch);
        },
		findData: (textSearch) => {
			TreeAction.findData(dispatch, textSearch);
		}
  	}
}

export default connect(mapStateToProps, mapAction)(ListTree)
