import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import Planting from '../planting';
import exampleData from './data';

class Example extends Component {

    constructor(props, context) {
        super(props);

        this.state = {
			plant: {},
            tillers: []
        }
    }

    componentDidMount() {
        this.setPlant(this.props);
    }

    componentWillReceiveProps(newProps) {
        this.setPlant(newProps);
    }

    setPlant(newProps) {
        let params = newProps.params;
        let name = params.exampleName;
        let plant = exampleData[name];
        this.setState({
            plant: {
                _id: plant._id,
                name: plant.name
            },
            tillers: plant.prop
        });
    }

	render() {
        return (
			<Planting
                mode="view"
                plant={this.state.plant}
                tillers={this.state.tillers}
            />
		);
	}
}

export default Example
