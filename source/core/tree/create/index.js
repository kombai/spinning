import React, {Component, PropTypes} from 'react';
import Planting from '../planting';
import Helper from '../../../common/helper';

class CreateTree extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            tree: {}
        };
    }

    componentDidMount() {
        this.setState({
            tree: Helper.getTreeData()
        });
    }

	render() {
		return (
			<Planting
                mode="insert"
                tree={this.state.tree}
            />
		);
	}
}


export default CreateTree
