import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ReactDOM from 'react-dom';
import Helper from '../../../common/helper'


class Preview extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            tillers: [],
            marginTop: 0,
            heightContent: 0
        }

        this.maxHeight = 33553490; // limit by system: 33554428
        this.rowHeight = 0;
        this.rowIndex = 0;
        this.rowLimit = 100;
        this.scrollRatio = 1;
        this.totalHeight = 0;
        this.replaceHeight = 0;

        this.goBack = this.goBack.bind(this);
        this.onScroll = this.onScroll.bind(this);
    }

    componentWillMount() {
        // store all tillers;
        let tillers = this.props.tillers;
        let display = tillers.slice(0, this.rowLimit);
        this.setState({
            tillers: display
        });
    }

    componentDidMount() {
        // estimate the height of row and content;
        this.plantContent = ReactDOM.findDOMNode(this.refs.plantContent);
        let contentHeight = this.plantContent.offsetHeight;
        let rowHeight = contentHeight/this.rowLimit;
        let totalRow = this.props.tillers.length;
        this.totalHeight = rowHeight * totalRow;
        let totalHeight = this.totalHeight;

        if (this.totalHeight > this.maxHeight) {
            this.scrollRatio = totalHeight/this.maxHeight;
            totalHeight = this.maxHeight;
        }
        this.rowHeight = rowHeight;

        this.setState({
            heightContent: totalHeight
        });

        console.log("total records", totalRow);
    }

	goBack() {
		this.context.router.goBack();
	}

    onScroll() {

        let scrollTop = this.content.scrollTop * this.scrollRatio;
        let current = Math.floor(scrollTop/this.rowHeight);
        let rowRef = Math.floor(current/10) * 10;
        let rowStart = (rowRef - 10);
        if (rowStart < 0) {
            rowStart = 0;
        }
        let tillers = this.props.tillers;
        let subMargin = this.content.scrollTop - scrollTop;

        if (rowStart != this.rowIndex) {
            let displayTillers = tillers.slice(rowStart, rowStart + 30);

            this.setState({
                tillers: displayTillers
            });

            this.rowIndex = rowStart;
            this.replaceHeight = rowStart * this.rowHeight;
        }

        this.plantContent.style.marginTop = subMargin + this.replaceHeight + "px";
    }

	render() {
        let tillers = this.state.tillers;

		return (
			<div className="germinate-page">
                <div className="page-content container-fluid">
    				<div className="page-header">
                        <h2> Preview </h2>
                    </div>
                    <div className="page-body" onScroll={this.onScroll} ref={(ref) => {this.content = ref}}>
                        <div className="height-content" style={{height: this.state.heightContent}}>
                            <div className="plant-content" ref="plantContent">
                                {
                					tillers.map(function(tiller, tillerIndex) {
                                        if (!tiller.length) {
                                            return null;
                                        }
                                        let index = this.rowIndex + tillerIndex;
                						return (
                							<div key={index} className={index + " panel panel-success"}>
                								<div className="panel-heading">
                                                    <span className="glyphicon glyphicon-grain"></span>
                                                    <span className="title-num">-{index}</span>
                                                </div>
                                                <ul className="list-group">
                                                {
                                                    tiller.length ? tiller.map(function(leaf, leafIndex) {
                                                        return (
                                                            <li className="leaf-group" key={index + "-" + leafIndex}>
                                                                <div className="leaf-name">
                                                                    {leaf.name}
                                                                </div>
                                                                <div className="leaf-value">
                                                                    {leaf.value}
                                                                </div>
                                                            </li>
                                                        );
                                                    }.bind(this)) : null
                                                }
                                                </ul>
                							</div>
                						);
                					}.bind(this))
                				}
                            </div>
    		            </div>
                    </div>
                    <div className="page-footer">
                        <button className="btn btn-lg btn-success" onClick={this.goBack}>Back</button>
    				</div>
                </div>
                <div className="page-extend"></div>
            </div>
		);
	}
}

Preview.contextTypes = {
    router: PropTypes.object.isRequired
}

function mapStateToProps(state, thisProps) {
	return {
		tillers: state.plant.tillers
	}
}

export default connect(mapStateToProps)(Preview)
