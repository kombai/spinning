import treeState from './state'

function reducer(state = treeState, action) {
    switch (action.type) {
        case 'LOAD-TREE':
            return Object.assign({}, state, {
                type: action.type,
                trees: action.trees
            });
        case 'FIND-TREE':
            return Object.assign({}, state, {
                type: action.type,
                trees: action.trees
            });
        case 'INSERT-TREE':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'UPDATE-TREE':
            return Object.assign({}, state, {
                type: action.type
            });
        case 'REMOVE-TREE':
            return Object.assign({}, state, {
                type: action.type
            });
        default:
            return state
    }
}

export default reducer
