
import Helper from '../../../common/helper';
import dataStore from './store-data';

const TreeAction = {

    loadData(dispatch) {
        dataStore.find({} , function(err, docs) {
			dispatch({
                type: "LOAD-TREE",
                trees: docs
            });
		});
    },

    findData(dispatch, textSearch) {
        dataStore.find({name: new RegExp(textSearch, 'i')} , function(err, docs) {
            dispatch({
                type: "FIND-TREE",
                trees: docs
            });
		});
    },

    insert(dispatch, tree) {
        dataStore.insert(tree, function(err, newDoc) {
            dispatch({
                type: "INSERT-TREE"
            });
        });
    },

    update(dispatch, tree) {
        dataStore.update({_id: tree._id }, {$set: tree}, {}, function (err) {
            dispatch({
                type: "UPDATE-TREE"
            });
        });
    },

    remove(dispatch, tree) {
        dataStore.remove({_id: tree._id}, {}, function(err) {
            dispatch({
                type: "REMOVE-TREE"
            });
        });
    }
}

export default TreeAction
