let Datastore = electronRequire('nedb');

let treeData = new Datastore({
    filename: './storage/data-store/tree-data.db'
});

export default treeData;
