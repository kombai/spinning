import React, {Component, PropTypes} from 'react';
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';
import TinyAction from './tiny-action';
import TinyService from './tiny-service';

class Preview extends Component {

    constructor(props, context) {
        super(props, context);
        let location = props.location;
        this.state = {
            total: 0,
            twigs: [],
            treeData: location.state
        };
    }

    componentDidMount() {
        console.log("TinyService", TinyService);
        TinyAction.grafting(this.state.treeData, function(instantData) {
            console.log("instantData", instantData);
        });
    }

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
    }

    onDataChange() {
        let plantingState = TinyStore.getState();
		let actionType = plantingState.type;
        let twigs = plantingState.twigs;

		if (actionType === "GRAFTING") {
            this.setState({
                twigs: twigs,
                total: twigs.length
            });
        }
    }

	render() {
        let treeData = this.state.treeData;
        let twigs = this.state.twigs;
		return (
			<div className="field-page page">
                <div className="page-content container-fluid">
                    <div className="page-header">
                        <h2>{treeData ? treeData.name : null}</h2>
                    </div>
                    <div className="page-body">
                    {
                        twigs.length < 1 ?
                            <h4 className="text-center">processing... </h4>
                            :
                            null
                    }

                    {
                        twigs.length < 1 && this.state.total > 1 ?
                            <h4 className="text-center">{this.state.total} twigs have been created</h4>
                            :
                            null
                    }

                    {
                        twigs.length ? twigs.map(function(leaves, index) {

        					return (
        						<div key={index} className="panel panel-success">
        							<div className="panel-heading">
                                        <span className="glyphicon glyphicon-grain"></span>
                                    </div>
                                    <ul className="list-group">
                                    {
                                        leaves.length ? leaves.map(function(leaf, leafIndex) {
                                            let customeStyle = null;
                                            let leafContent = null;
                                            if (leaf.type === "image") {
                                                customeStyle = {
                        							backgroundImage: "url(" + Helper.getImagePath(leaf.value) + ")"
                        						};
                                                leafContent = (<div className="photo" style={customeStyle}></div>);
                                            } else if (leaf.type === "none") {
                                                leafContent = (<div className="value">{leaf.value}</div>);
                                            }

                                            return (
                                                <li className="list-group-item" key={index + "-" + leafIndex}>
                                                    <div className="caption pull-left">{leaf.name}</div>
                                                    {leafContent}
                                                </li>
                                            );
                                        }.bind(this)) : null
                                    }
                                    </ul>
        						</div>
        					);
        				}.bind(this)) : null
                    }
                    </div>

                    <div className="page-footer">
                        <button className="btn btn-lg btn-default pull-right" onClick={() => this.context.router.goBack()}>Cancel</button>
    				</div>

                </div>
                <div className="page-extend">
                </div>
		    </div>
		);``
	}
}


Preview.contextTypes = {
    router: PropTypes.object.isRequired
}


export default Preview
