
import Helper from '../../../common/helper';
import TinyStore from './tiny-store';

const grafting = (rootStock, scion, callback) => {
    let twigs = [];
    let b, e;
    let sl = scion.length;
    let rSl = rootStock.length;

    for (b = 0; b < rSl; ++b) {

        for (e = 0; e < sl; ++e) {

            let newTwig = [].concat(rootStock[b]);

            if (scion[e].value != null) {
                newTwig = newTwig.concat(scion[e]);
            }

            if (callback) {
                try {
                    callback(newTwig);
                } catch(err) {
                    console.log("callback error", err);
                }
            }

            newTwig && twigs.push(newTwig);
        }

    }

    return twigs.length ? twigs : scion;
};


const germinate = (twig) => {
    let leaves = [];
    if (twig && twig.value) {
        leaves = twig.value.map((leaf, index) => {
            return {
                name: twig.name,
                type: leaf.type,
                value: leaf.value
            }
        });
    }
    return leaves;
};

const TinyAction = {

    grafting(data, callback) {

        let longTerm = data.value.longTerm;
        let twigs = [[]];
        let lastRound = {};
        do {
            lastRound = longTerm.pop();
        } while(!lastRound.value.length)

        longTerm.forEach((item, index) => {
            if (item && item.value.length) {
                twigs = grafting(twigs, germinate(item));
            }
        });
        // last round;
        if (lastRound && lastRound.value.length) {
            twigs = grafting(twigs, germinate(lastRound), callback);
        }

        TinyStore.dispatch({
            type: "GRAFTING",
            twigs: twigs,
            total: twigs.length
        });
    }
}


export default TinyAction
