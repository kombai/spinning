import React, {Component, PropTypes} from 'react';

class FilterResult extends Component {

	constructor(props, context) {
	}

	componentWillMount() {
        this.onDataChange = this.onDataChange.bind(this);
        this.unsubscribe = TinyStore.subscribe(this.onDataChange);
    }

    componentWillUnmount() {
       this.unsubscribe();
	   this.onDataChange = null;
    }

    onDataChange() {
        let twigState = TinyStore.getState();
    }


	render() {
		if (this.state.visible == false) {
			return null;
		}

		return (
			<div className="modal">
				<div className="content">
					<div className="modal-body">
						<div className="form-group">
							<h3 className="modal-title">Filter Result</h3>
						</div>
						<div className="form-group">

						</div>
					</div>
					<div className="modal-footer">
						<button className="btn btn-primary" onClick={this.applyFilterResult}>Apply</button>
						<button className="btn btn-default pull-right" onClick={this.cancelFilterResult}>Close</button>
					</div>
				</div>
			</div>
		);
	}
}

export default FilterResult
