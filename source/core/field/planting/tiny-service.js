
let listRequire = require.context("../service", false, /\.js$/);
let tinyService = {};

listRequire.keys().forEach(function(item, index) {
    let action = listRequire(item).default;
    tinyService[action.name] = action.value;
});

export default tinyService;
