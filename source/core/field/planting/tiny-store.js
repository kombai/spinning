import {createStore} from 'redux';

let tinyState = {
    type: "",
    total: 0,
    twigs: []
};

function reducer(state = tinyState, action) {
    switch (action.type) {
        case 'GRAFTING':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs,
                total: action.total
            });

        case 'UPDATE-TWIG':
            return Object.assign({}, state, {
                type: action.type,
                twigs: action.twigs
            });

        default:
            return state
    }
}

export default createStore(reducer)
