import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

class Menu extends Component {

	render() {

		return (
			<div className="menu-bar">

				<div className="add">
					<div className="cover">
						<Link className="link" to={{pathname: "create-leaf", state: null}}>
							<span className="glyphicon glyphicon-leaf"></span>
						</Link>
					</div>

					<div className="cover">
						<Link className="link" to={{pathname: "create-twig", state: null}}>
							<span className="glyphicon glyphicon-grain"></span>
						</Link>
					</div>

					<div className="cover">
						<Link className="link" to={{pathname: "create-tree", state: null}}>
							<span className="glyphicon glyphicon-tree-conifer"></span>
						</Link>
					</div>
				</div>

				<div className="list">
					<div className="cover">
						<div className="link">
							<Link to={{pathname: "list-leaf", state: null}}>Leaf list</Link>
						</div>
					</div>

					<div className="cover">
						<div className="link">
							<Link to={{pathname: "list-twig", state: null}}>Twig list</Link>
						</div>
					</div>

					<div className="cover">
						<div className="link">
							<Link to={{pathname: "list-tree", state: null}}>Tree list</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Menu
