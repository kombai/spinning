import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import React from 'react';

import TreeData from '../../core/tree/connect/store-data';
import LeafData from '../../core/leaf/connect/store-data';
import TwigData from '../../core/twig/connect/store-data';

import MainLayout from '../layout';

import ListLeaf from '../../core/leaf/list';
import EditLeaf from '../../core/leaf/edit';
import CreateLeaf from '../../core/leaf/create';

import ListTwig from '../../core/twig/list';
import EditTwig from '../../core/twig/edit';
import CreateTwig from '../../core/twig/create';

import ListTree from '../../core/tree/list';
import EditTree from '../../core/tree/edit';
import CreateTree from '../../core/tree/create';

import PreviewResult from '../../core/field/planting';

import reduceLeaf from '../../core/leaf/connect/reduce';
import reduceTwig from '../../core/twig/connect/reduce';
import reduceTree from '../../core/tree/connect/reduce';

let combineReducer = combineReducers({
    leaf: reduceLeaf,
    twig: reduceTwig,
    tree: reduceTree
});

let combineStore = createStore(combineReducer)

LeafData.loadDatabase();
TwigData.loadDatabase();
TreeData.loadDatabase(function(err) {

    // Render to index.html
    ReactDOM.render((
        <Provider store={combineStore}>
            <Router history={hashHistory}>
                <Route path="/" component={MainLayout}>
                    <IndexRoute component={ListTree}/>

                    <Route path="create-leaf" component={CreateLeaf} />
                    <Route path="list-leaf" component={ListLeaf} />
                    <Route path="edit-leaf/:leafId" component={EditLeaf} />

                    <Route path="create-twig" component={CreateTwig} />
                    <Route path="list-twig" component={ListTwig} />
                    <Route path="edit-twig/:leafId" component={EditTwig} />

                    <Route path="create-tree" component={CreateTree} />
                    <Route path="list-tree" component={ListTree} />
                    <Route path="edit-tree/:treeId" component={EditTree} />

                    <Route path="preview-result" component={PreviewResult} />
                </Route>
            </Router>
        </Provider>
    ), document.getElementById('electron-container'));

});
