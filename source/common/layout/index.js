import React, {Component, PropTypes} from 'react';
import Menu from '../menu'

class Layout extends Component {

	render() {
		return (
			<div className="layout">
				<div className="main">
					{this.props.children}
				</div>

				<div className="menu-block">
					<Menu />
				</div>
		    </div>
		)
	}
}


export default Layout
