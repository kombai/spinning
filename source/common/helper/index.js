const remote = electronRequire("electron").remote;
const storePath = remote.getGlobal("storePath");

export default {

	photoStore: storePath + "/photo-store/",

	getGUID() {
		var hexcodes = "0123456789abcdef".split("");
		var result = '';
		for (var index = 0; index < 32; index++) {
			var value = Math.floor(Math.random() * 16);
			switch (index) {
			case 8:
				result += '-';
				break;
			case 12:
				value = 4;
				result += '-';
				break;
			case 16:
				value = value & 3 | 8;
				result += '-';
				break;
			case 20:
				result += '-';
				break;
			}
			result += hexcodes[value];
		}
		return result;
	},

	getImagePath(leafValue) {
		return this.photoStore + leafValue;
	},

	getTreeData() {
		return {
			_id: this.getGUID(),
			name: "New Tree",
			value: {
				longTerm: [{
					name: "Chất liệu",
					value: []
				}],
				shortTerm: [{
					name: "Nguồn",
					value: []
				}, {
					name: "Đích",
					value: []
				}],
				temporary: [{
					name: "Lưu trữ",
					value: []
				}, {
					name: "Vận tải",
					value: []
				}, {
					name: "Kiểm tra",
					value: []
				}, {
					name: "Tác dụng",
					value: []
				} ]
			}
		};
	}
};
